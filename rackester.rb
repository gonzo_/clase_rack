# require 'rack'
require 'haml'

module Rackester

  module Viewer
    def load_file(file)
      begin
        File.read("./views/#{file}.haml")
      rescue
        p "No existe el archivo, MotherFucker!!!"
      end
    end

    def render(file_view)
      file = load_file(file_view)
      Haml::Engine.new(file).render
    end
  end 


  class App
    extend Rackester::Viewer

    @@routes = []

    def call(env) 
      @env = env
      @request = Rack::Request.new(env)
      @method = @request.request_method.downcase
      @route = @request.path_info
      process_call
		end

		def routes
			@@routes
		end

    def process_call
      proceso = @@routes.select {|r| r[0] == @method && r[1] == @route  }.first
      if proceso
        proceso[2].call(@request)
      else
        [404, {"Content-Type" => "text/html"}, [Haml::Engine.new(File.read("./views/404.haml")).render]]
      end
    end

    def self.save_route method, route, block
      @@routes << [method, route, block]
    end

    def self.put route, &block
      save_route 'put', route, block
    end
    def self.get route, &block
      save_route 'get', route, block
    end
    def self.post route, &block
      save_route 'post', route, block
    end
  end
end



class Aplicacion < Rackester::App
  
	get '/' do |env|
    [200, {"Content-Type" => "text/html"}, [self.render("home")]]
  end

	post '/prueba' do |env|
		hola = "Hola gonzo " + Time.now.to_s
		[200, {"Content-Type" => "text/plain"}, [hola]]
	end


end
